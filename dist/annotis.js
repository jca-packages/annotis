function Shape()
{
	this.x = 0;
	this.y = 0;
	this.cursor = "auto";

	this.setPixel = function(buffer, x, y, color)
	{
		var index = (Math.round(x) + Math.round(y) * buffer.width) * 4;
		var coef = color[3] / 255;
		for(var i = 0; i < 3; i++)
			buffer.data[index + i] = buffer.data[index + i] * (1 - coef) + color[i] * coef;
	}

	this.draw = function(buffer, color)
	{
		throw "Abstract method draw not implemented";
	}

	this.isInside = function(x, y)
	{
		throw "Abstract method isInside not implemented";
	}
}

function Group()
{
	Shape.call(this);

	this.children = [];

	this.draw = function(buffer, color)
	{
		this.children.forEach(shape =>
		{
			shape.draw(buffer, color);
		});
	}

	this.isInside = function(x, y)
	{
		for(var i = 0; i < this.children.length; i++)
			if(this.children[i].isInside(x, y))
				return true;
		return false;
	}
}

function Line(x, y, l, o = 'h')
{
	Shape.call(this);

	this.x = x;
	this.y = y;
	this.l = l;
	this.o = o;

	this.draw = function(buffer, color)
	{
		for(var i = 0; i < this.l; i++)
			if(this.o == 'h')
				this.setPixel(buffer, this.x + i, this.y, color);
			else if(this.o == 'v')
				this.setPixel(buffer, this.x, this.y + i, color);
	}

	this.isInside = function(x, y)
	{
		if(this.o == 'h' && this.x <= x && x <= (this.x + this.l) && this.y == y)
			return true;
		else if(this.o == 'v' && this.y <= y && y <= (this.y + this.l) && this.x == x)
			return true;
		return false;
	}
}

function Curve(x, y)
{
	Group.call(this);

	this.x = x;
	this.y = y;

	this.pushLine = function(x, y, l, o = 'h')
	{
		this.children.push(new Line(this.x + x, this.y + y, CORNER_SIZE, o));
	}
}

function Rectangle(x, y, w, h)
{
	Group.call(this);

	this.x = x;
	this.y = y;
	this.w = w;
	this.h = h;

	this.children.push(new Line(x, y, w));
	this.children.push(new Line(x + w, y, h, 'v'));
	this.children.push(new Line(x, y + h, w));
	this.children.push(new Line(x, y, h, 'v'));

	this.isInside = function(x, y)
	{
		return x >= this.x && x <= this.x + this.w && y >= this.y && y <= this.y + this.h;
	}
}

function Frame(x, y, w, h)
{
	Group.call(this);

	this.resize = function(x,y,w,h)
	{
		this.children = [];

		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;

		var topLeftCorner = new Curve(x - CORNER_OFFSET, y - CORNER_OFFSET);
		topLeftCorner.pushLine(0, 0, CORNER_SIZE);
		topLeftCorner.pushLine(0, 0, CORNER_SIZE, 'v');
		topLeftCorner.cursor = "nw-resize";
		this.children.push(topLeftCorner);

		var topRightCorner = new Curve(x + w + CORNER_OFFSET, y - CORNER_OFFSET);
		topRightCorner.pushLine(-CORNER_SIZE, 0, CORNER_SIZE);
		topRightCorner.pushLine(0, 0, CORNER_SIZE, 'v');
		topRightCorner.cursor = "ne-resize";
		this.children.push(topRightCorner);

		var bottomLeftCorner = new Curve(x - CORNER_OFFSET, y + h + CORNER_OFFSET);
		bottomLeftCorner.pushLine(0, 0, CORNER_SIZE);
		bottomLeftCorner.pushLine(0, 0 - CORNER_SIZE, CORNER_SIZE, 'v');
		bottomLeftCorner.cursor = "sw-resize";
		this.children.push(bottomLeftCorner);

		var bottomRightCorner = new Curve(x + w + CORNER_OFFSET, y + h + CORNER_OFFSET);
		bottomRightCorner.pushLine(0 - CORNER_SIZE, 0, CORNER_SIZE);
		bottomRightCorner.pushLine(0, 0 - CORNER_SIZE, CORNER_SIZE, 'v');
		bottomRightCorner.cursor = "se-resize";
		this.children.push(bottomRightCorner);

		var topCorner = new Curve(x + (w / 2), y - CORNER_OFFSET);
		topCorner.pushLine(0 - CORNER_SIZE / 2, 0, CORNER_SIZE);
		topCorner.cursor = "n-resize";
		this.children.push(topCorner);

		var rightCorner = new Curve(x + w + CORNER_OFFSET, y + (h / 2));
		rightCorner.pushLine(0, 0 - CORNER_SIZE / 2, CORNER_SIZE, 'v');
		rightCorner.cursor = "e-resize";
		this.children.push(rightCorner);

		var bottomCorner = new Curve(x + (w / 2), y + h + CORNER_OFFSET);
		bottomCorner.pushLine(0 - CORNER_SIZE / 2, 0, CORNER_SIZE);
		bottomCorner.cursor = "s-resize";
		this.children.push(bottomCorner);

		var leftCorner = new Curve(x - CORNER_OFFSET, y + (h / 2) - CORNER_SIZE / 2);
		leftCorner.pushLine(0, 0, CORNER_SIZE, 'v');
		leftCorner.cursor = "w-resize";
		this.children.push(leftCorner);

		var rectangle = new Rectangle(x, y, w, h);
		rectangle.cursor = "pointer";
		this.children.push(rectangle);
	}

	this.resize(x,y,w,h);
}

// Consts
var CORNER_OFFSET = 3;
var CORNER_SIZE = 4;

function Annotis(source)
{
	var ratio = source.naturalWidth/source.naturalHeight;
	var width = source.height * ratio;
	var height = source.height;
	if (width > source.width) {
		width = source.width;
		height = source.width/ratio;
	}

	this.source = source;

	this.canvas = document.createElement('canvas');
	this.canvas.style.cursor = 'pointer';

	this.canvas.id = source.id;
	this.canvas.width = width;
	this.canvas.height = height;
	this.canvas.width = width;
	this.canvas.style.width = width + "px";
	this.canvas.style.height = height + "px";
	this.canvas.style.cursor = "auto";
	source.parentElement.appendChild(this.canvas);
	
	Object.keys( source.dataset ).forEach(k => {
		this.canvas.dataset[k] = source.dataset[k];
	});

	source.parentElement.removeChild(this.source);

	this.ctx = this.canvas.getContext('2d');
	// Shape properties
	this.ctx.strokeStyle = "#0000FF";
	this.ctx.fillStyle = 'rgba(225,225,225,0.5)';
	this.ctx.globalAlpha = 1.0;
	this.ctx.lineWidth = 1;

	this.ctx.translate(0.5, 0.5);
	this.shapes = [];
	this.start_x = 0;
	this.start_y = 0;
	this.state = "idle";
	this.isDrawing = false;
	this.isDragging = false;
	this.over = null;
	this.active = null;
	this.selected = null;
	this.onnewshape = null;
	this.pixelColor = [0, 0, 255, 255];
	this.activePixelColor = [0, 255, 255, 255];

	var _this = this;

	// Shapes draw
	this.redraw = function()
	{
		// Buffer draw
		this.ctx.drawImage(this.source, 0, 0, this.source.naturalWidth, this.source.naturalHeight, 0, 0, this.canvas.width, this.canvas.height);
		this.buffer = this.ctx.getImageData(0, 0, this.canvas.clientWidth, this.canvas.clientHeight);

		for (const shape of this.shapes)
			shape.draw(this.buffer, shape == this.active ? this.activePixelColor  : this.pixelColor);

		// Buffer draw
		this.ctx.putImageData(this.buffer, 0, 0);
	};

	this.getCanvasPosition = function(rect, event)
	{
		return {x: event.clientX - rect.left, y: event.clientY - rect.top};
	};

	// Canvas events
	this.canvas.onmousedown = function(event){
		_this.mousedown(this, _this.getCanvasPosition(this.getBoundingClientRect(), event));
	}

	this.mousedown = function(canvas, coords)
	{
		this.start_x = coords.x;
		this.start_y = coords.y;

		this.state = 'touched';
	}

	this.canvas.onmousemove = function(event){
		_this.mousemove(this, _this.getCanvasPosition(this.getBoundingClientRect(), event));
	}

	this.getShapeAt = function(x, y, shapes)
	{
		for(var i = 0; i < shapes.length; i++)
		{
			var shape = shapes[i];
			var type = shape.constructor.name;
			if(type == 'Frame')
			{
				var subshape = this.getShapeAt(x, y, shape.children);
				if(subshape != null)
				{
					this.active = shape;
					return subshape;
				}
			}
			else
			{
				if( shape.isInside(x, y) )
					return shape;
			}
		}
	}

	this.mousemove = function(canvas, coords)
	{
		if(this.state == "idle")
		{
			canvas.style.cursor = 'auto';
			
			var shape = this.getShapeAt(coords.x, coords.y, this.shapes);
			if(shape)
			{
				canvas.style.cursor = shape.cursor;
				this.redraw();
			}
			else if(this.active != null)
			{
				this.active = null;
				this.redraw();
			}	
		}

		if(this.state == "touched")
		{
			var delta_x = coords.x-this.start_x;
			var delta_y = coords.y-this.start_y;

			if(this.canvas.style.cursor == "nw-resize")
				this.active.resize(this.active.x + delta_x, this.active.y + delta_y, this.active.w - delta_x, this.active.h - delta_y);
			if(this.canvas.style.cursor == "n-resize")
				this.active.resize(this.active.x, this.active.y + delta_y, this.active.w, this.active.h - delta_y);
			if(this.canvas.style.cursor == "ne-resize")
				this.active.resize(this.active.x, this.active.y + delta_y, this.active.w + delta_x, this.active.h - delta_y);
			if(this.canvas.style.cursor == "e-resize")
				this.active.resize(this.active.x, this.active.y, this.active.w + delta_x, this.active.h);
			if(this.canvas.style.cursor == "se-resize")
				this.active.resize(this.active.x, this.active.y, this.active.w + delta_x, this.active.h + delta_y);
			if(this.canvas.style.cursor == "s-resize")
				this.active.resize(this.active.x, this.active.y, this.active.w, this.active.h + delta_y);
			if(this.canvas.style.cursor == "sw-resize")
				this.active.resize(this.active.x + delta_x, this.active.y, this.active.w - delta_x, this.active.h + delta_y);
			if(this.canvas.style.cursor == "w-resize")
				this.active.resize(this.active.x + delta_x, this.active.y, this.active.w - delta_x, this.active.h);
			if(this.canvas.style.cursor == "pointer")
				this.active.resize(this.active.x + delta_x, this.active.y + delta_y, this.active.w, this.active.h);

			this.start_x = coords.x;
			this.start_y = coords.y;

			this.redraw();
		}
	}

	this.canvas.onmouseup = function(event){
		_this.mouseup(this, _this.getCanvasPosition(this.getBoundingClientRect(), event));
	}

	this.mouseup = function(canvas, coords)
	{
		this.state = "idle";
	}

	this.canvas.ontouchstart = function(event){
		_this.mousedown(this, _this.getCanvasPosition(this.getBoundingClientRect(), event.touches[0]));
	}
	this.canvas.ontouchmove = function(event){
		_this.mousemove(this, _this.getCanvasPosition(this.getBoundingClientRect(), event.touches[0]));
	}
	this.canvas.ontouchend = function(event){
		_this.mouseup(this, _this.getCanvasPosition(this.getBoundingClientRect(), event.touches[0]));
	}

	this.redraw();
}

// 361